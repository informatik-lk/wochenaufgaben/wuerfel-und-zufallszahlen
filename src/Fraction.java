public class Fraction {
  private int num, denom;   // numerator and denominator of the fraction

  // This constructor takes a numerator and denominator and creates a fraction
  public Fraction(int numerator, int denominator) {    
    num   = numerator;
    denom = denominator;
  }

  // This constructor takes a string of the form "numerator/denominator" and creates a fraction from it
  public Fraction(String fraction) {
    String[] fracValues = fraction.split("/");
    num = Integer.parseInt(fracValues[0]);
    denom = Integer.parseInt(fracValues[1]);
  }

  // Accessor methods
  public int getNumerator() { return num;  }
  public int getDenominator() { return denom; }

  // The toFloat() method returns the value of the fraction as a floating point number
  public float toFloat()     { return ((float)num / (float)denom); }

  // The toString() method returns the value of the fraction as a string of the form "numerator/denominator"
  public String toString()    { return denom == 0 ? null : num == 0 ? "0" : num == denom ? "1" : String.format("%s/%s", num, denom); }

  // The equals() method returns true if the fraction is equal to the argument
  public boolean equals(Fraction f) {
    return this.toFloat() == f.toFloat();
  }

  // The reduce() method reduces the fraction to its lowest terms
  public void reduce() {
    // Declare a temporary numerator and denominator by dividing the numerator and denominator by their greatest common factor
    int tempNum = num / gcf(num, denom);
    int tempDenom = denom / gcf(num, denom);

    // Set the numerator and denominator to the temporary values
    num = tempNum;
    denom = tempDenom;
  }

  // The add() method adds two fractions together and returns the result as a new fraction
  public Fraction add(Fraction b) {
    int newDenom = lcm(denom, b.getDenominator()); // Find the least common multiple of the denominators
    int newNum = (newDenom / denom) * num + (newDenom / b.getDenominator()) * b.getNumerator(); // Add the numerators together
    return new Fraction(newNum, newDenom); // Return the result as a new fraction
  }

  // The gcf() method returns the greatest common factor of two integers using the Euclidean algorithm recursively
  private int gcf(int a, int b) {
    if (a < b) {
      return gcf(b,a);
    } else if (a == b) {
      return a;
    }

    int c = a / b;
    int r = a % b;

    if (r == 0) {
      return b;
    } else {
      return gcf(b,r);
    }
  }

  // The lcm() method returns the least common multiple of two integers
  private int lcm(int a, int b) {
    return (a * b) / gcf(a,b);
  }
}

