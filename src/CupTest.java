class CupTest {
   public static void main(String[] args) {
        // Declare a new cup of dice
        CupOfDice cup = new CupOfDice(0);
        // Add n default dice to the cup
        int n = 5;
        for (int i = 0; i < n; i++) {
            cup.add(new Die(1, 6));
        }
        // Roll the cup
        System.out.println(cup.roll());
   }
}
