class DieTest {
   public static void main(String args[]) {
      // Declare occurence array and default die.
      int[] occurences = {0, 0, 0, 0, 0, 0};
      Die die = new Die(1,6);
      

      // Count occurences of all values when die is rolles n times.
      for (int i = 0; i < 100000; i++) {
         int n = die.roll();

         occurences[n-1]++;
      }

      // Declare an acceptable deviation margin.
      double deviation = 0.05;

      // Check if all values are within the acceptable deviation margin.
      for (int i = 0; i < occurences.length; i++) {
          double expected = (double) 100000 / 6;
          double actual = occurences[i];
          double difference = Math.abs(expected - actual);
          double percentage = difference / expected;

          if (percentage > deviation) {
             System.out.println("Die is probably not fair.");
             return;
          }
      }
      System.out.println("Die is probably fair.");
    }
}
