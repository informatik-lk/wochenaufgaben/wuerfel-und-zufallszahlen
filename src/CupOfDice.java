import java.util.ArrayList;
public class CupOfDice {
    // Declare cup as an ArrayList of Die objects.
    ArrayList<Die> cup = new ArrayList<Die>();

    // Declare constructor that takes the number of dice in the cup as an argument and adds that many dice to the cup.
    public CupOfDice(int numberOfDice) {
        for (int i = 0; i < numberOfDice; i++) {
            cup.add(new Die());
        }
    }

    // Roll method rolls each die in the cup and returns the sum of all the rolls.
    public int roll() {
        int total = 0;
        for (Die die : cup) {
            total += die.roll();
        }
        return total;
    }

    // add method adds an object, provided as a parameter die to the cup.
    public void add(Die die) {
        cup.add(die);
    }

    // remove method removes the die at the specified index from the cup.
    public void remove(int index) {
        cup.remove(index);
    }
}
