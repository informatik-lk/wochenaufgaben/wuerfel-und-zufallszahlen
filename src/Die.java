import java.util.Random;

class Die {
  // Declare rng and limits
  private Random rng;
  private int max, min;
  
  // Declare default constructor
  public Die() {
    rng = new Random();
    this.min = 1;
    this.max = 6;
  }

  // Declare constructor for custom limits
  public Die(int min, int max) { 
    rng = new Random();
    this.min = min;
    this.max = max;
  }

  // Roll method returns a random number within the given limits
  public int roll() {
    double n = rng.nextDouble()*(max-min+1)+min;
    return (int)n;
  }
}

